package client

import (
	"log"
	"os"
	"v2ray.com/core"
	"v2ray.com/core/app/dispatcher"
	"v2ray.com/core/app/proxyman"
	"v2ray.com/core/app/router"
	"v2ray.com/core/common/net"
	"v2ray.com/core/common/protocol"
	"v2ray.com/core/common/serial"
	"v2ray.com/core/proxy/dokodemo"
	"v2ray.com/core/proxy/freedom"
	"v2ray.com/core/transport/internet"
	"v2ray.com/core/transport/internet/tls"
	"v2ray.com/core/transport/internet/websocket"

	_ "v2ray.com/core/app/proxyman/inbound"
	_ "v2ray.com/core/app/proxyman/outbound"
)

type ServerConfig struct {
	Address string
	Port    net.Port
}

type V2rayClient struct {
	Local  ServerConfig
	Remote []ServerConfig

	Host string

	Path string

	Mux uint

	instance *core.Instance
}

func (c *V2rayClient) Start() {
	transportSettings := &websocket.Config{
		Path: c.Path,
		Header: []*websocket.Header{
			{Key: "Host", Value: c.Host},
		},
	}
	connectionReuse := true

	streamConfig := internet.StreamConfig{
		ProtocolName: "websocket",
		TransportSettings: []*internet.TransportConfig{{
			ProtocolName: "websocket",
			Settings:     serial.ToTypedMessage(transportSettings),
		}},
	}

	// Fast-open
	streamConfig.SocketSettings = &internet.SocketConfig{Tfo: internet.SocketConfig_Enable}

	tlsConfig := tls.Config{ServerName: c.Host}

	streamConfig.SecurityType = serial.GetMessageType(&tlsConfig)
	streamConfig.SecuritySettings = []*serial.TypedMessage{serial.ToTypedMessage(&tlsConfig)}

	apps := []*serial.TypedMessage{
		serial.ToTypedMessage(&dispatcher.Config{}),
		serial.ToTypedMessage(&router.Config{
			Rule: []*router.RoutingRule{
				{
					Networks: []net.Network{net.Network_TCP, net.Network_UDP},
					TargetTag: &router.RoutingRule_BalancingTag{
						BalancingTag: "balancer",
					},
				},
			},
			BalancingRule: []*router.BalancingRule{
				{
					Tag:              "balancer",
					OutboundSelector: []string{"_"},
				},
			}}),
		serial.ToTypedMessage(&proxyman.InboundConfig{}),
		serial.ToTypedMessage(&proxyman.OutboundConfig{}),
	}

	senderConfig := proxyman.SenderConfig{StreamSettings: &streamConfig}
	if connectionReuse {
		senderConfig.MultiplexSettings = &proxyman.MultiplexingConfig{Enabled: true, Concurrency: uint32(c.Mux)}
	}

	var outboundHandlerConfigs []*core.OutboundHandlerConfig

	for _, remote := range c.Remote {
		outboundProxy := serial.ToTypedMessage(&freedom.Config{
			DestinationOverride: &freedom.DestinationOverride{
				Server: &protocol.ServerEndpoint{
					Address: net.NewIPOrDomain(net.ParseAddress(remote.Address)),
					Port:    uint32(remote.Port),
				},
			},
		})

		outboundHandlerConfig := &core.OutboundHandlerConfig{
			Tag:            "_" + remote.Address,
			SenderSettings: serial.ToTypedMessage(&senderConfig),
			ProxySettings:  outboundProxy,
		}

		outboundHandlerConfigs = append(outboundHandlerConfigs, outboundHandlerConfig)
	}

	config := &core.Config{
		Inbound: []*core.InboundHandlerConfig{{
			ReceiverSettings: serial.ToTypedMessage(&proxyman.ReceiverConfig{
				PortRange: net.SinglePortRange(c.Local.Port),
				Listen:    net.NewIPOrDomain(net.ParseAddress(c.Local.Address)),
			}),
			ProxySettings: serial.ToTypedMessage(&dokodemo.Config{
				Address:  net.NewIPOrDomain(net.LocalHostIP),
				Networks: []net.Network{net.Network_TCP},
			}),
		}},
		Outbound: outboundHandlerConfigs,
		App:      apps,
	}

	instance, err := core.New(config)
	if err != nil {
		panic(err)
	}

	if err := instance.Start(); err != nil {
		log.Println("failed to start server:", err.Error())
		os.Exit(1)
	}

	c.instance = instance
}

func (c *V2rayClient) Stop() {
	if c.instance != nil {
		c.instance.Close()
	}
}
